+++
title = "新站点搭建成功"
description = ""
draft = false

# YYYY-MM-DD (2012-10-02)
# RFC3339 (2002-10-02T15:00:00Z)
date = "2017-10-25"

#slug = ""
path = "hi"

# An array of strings allowing you to group pages with them
#tags = ['demo']

#category = "rust"
order = 0
weight = 0

# Template to use to render this page
template = "page.html"

[extra]
+++

还有许多要做的

- 更改首页到类似阮一峰的根页面那样子，里面再列出其他的blog等子section，文章都放到子section里面
- 主题优化，以简单美观为主
- 熟悉配置选项
    - 如果引用自定义变量
    - 变量层级是怎样的，这决定了变量之间如何覆盖来自定义
- 熟悉模板语法
- 分类和标签模板的编写
- 希望编写一个展示照片的页面
- 能参与到这个项目源代码编写就更好了
    - 还没有自动热更新功能
    - 偶然报一些panic错误